//
//  VideoStore.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 27/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

enum VideoResults {
    case success([Video])
    case failure(Error)
}

enum ImagesResult {
    case success(UIImage)
    case failure(Error)
}

enum ThumbnailError: Error {
    case thumbnailCreationError
}

class VideoStore {
    
    let imageStore = ImageStore()

    // Setup URLSession Configuration
    private let sessions: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    func fetchSearchVideos(matching query: String, completion: @escaping (VideoResults) -> ()) {
        let url = YoutubeAPI.youtubeURL(withAdditionalQuery: query)
        
        let request = URLRequest(url: url)
        let task = sessions.dataTask(with: request) { (data, response, error) in
            let result = self.processVideoRequest(data: data, error: error)
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        task.resume()
    }
    
    private func processVideoRequest(data: Data?, error: Error?) -> VideoResults {
        guard let jsonData = data else {
            return .failure(error!)
        }
        return YoutubeAPI.videos(fromJSON: jsonData)
    }
    
    func fetchImage(for video: Video, completion: @escaping (ImagesResult) -> ()) {
        guard let videoKey = video.videoID else {
            preconditionFailure("Video expected to have a video id")
        }
        
        if let image = imageStore.image(forKey: videoKey) {
            OperationQueue.main.addOperation {
                completion(.success(image))
            }
            return
        }
        
        guard let videoURL = video.url else {
            preconditionFailure("Video expected to have video url")
        }
                
        let request = URLRequest(url: videoURL)
        
        let task = sessions.dataTask(with: request) { (data, response, error) in
            let result = self.processImageRequest(data: data, error: error)
            
            if case let .success(image) = result {
                self.imageStore.setImage(image, forKey: videoKey)
            }
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        task.resume()
    }
    
    private func processImageRequest(data: Data?, error: Error?) -> ImagesResult {
        guard
            let imageData = data,
            let image = UIImage(data: imageData) else {
                if data == nil {
                    return .failure(error!)
                } else {
                    return .failure(ThumbnailError.thumbnailCreationError)
                }
            }
        return .success(image)
    }
}
