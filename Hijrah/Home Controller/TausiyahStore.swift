//
//  TausiyahStore.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 22/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class TausiyahStore: DataManager {
        
    var items = [TausiyahItem]()
    
    // Fetch data from plist
    func fetch() {
        for data in load(file: "TausiyahData") {
            items.append(TausiyahItem(dict: data))
        }
    }
}
