//
//  TausiyahItem.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 22/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

struct TausiyahItem {
    let name: String?
    let image: String?
}

extension TausiyahItem {
    init(dict: [String: AnyObject]) {
        self.name = dict["name"] as? String
        self.image = dict["image"] as? String
    }
}
