//
//  Video.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 23/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

enum SerializationError: Error {
    case missing(String)
}

class Video: NSCoding {
    var videoID: String?
    var title: String?
    var description: String?
    var url: URL?
    
    init(videoID: String, title: String, description: String, url: URL) {
        self.videoID = videoID
        self.title = title
        self.description = description
        self.url = url
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(videoID, forKey: "videoID")
        coder.encode(title, forKey: "title")
        coder.encode(description, forKey: "description")
        coder.encode(url, forKey: "url")
    }
    
    required init?(coder: NSCoder) {
        videoID = coder.decodeObject(forKey: "videoID") as! String
        title = coder.decodeObject(forKey: "title") as! String
        description = coder.decodeObject(forKey: "description") as! String
        url = coder.decodeObject(forKey: "url") as! URL
    }
}

extension Video {
    convenience init?(dict: [String: Any]) {
        guard
            let id = dict["id"] as? [String: String],
            let videoID = id["videoId"]
            else {
                return nil
        }

        guard
            let snippet = dict["snippet"] as? [String: Any],
            let description = snippet["description"] as? String,
            let title = snippet["title"] as? String,
            let thumbnails = snippet["thumbnails"] as? [String: Any],
            let medium = thumbnails["medium"] as? [String: Any],
            let urlString = medium["url"] as? String,
            let url = URL(string: urlString) else { return nil }

        self.init(videoID: videoID, title: title, description: description, url: url)
    }
}

extension Video: Equatable {
    static func == (lhs: Video, rhs: Video) -> Bool {
        return lhs.videoID == rhs.videoID
    }
}
