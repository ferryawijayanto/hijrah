//
//  VideoCell.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 25/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import JGProgressHUD

class VideoCell: UITableViewCell {
    
    @IBOutlet weak var videoImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    var progressHUD: JGProgressHUD!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        update(with: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        update(with: nil)
    }

    func update(with image: UIImage?) {
        if let imageToDisplay = image {
            videoImgView.image = imageToDisplay
        } else {
            videoImgView.image = nil
        }
    }
}
