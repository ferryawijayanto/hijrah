//
//  VideoPlayerView.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 30/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class VideoPlayerView: UIView {

    @IBOutlet weak var playerView: WKYTPlayerView!
    
    var panGesture: UIPanGestureRecognizer!
    
    var video: Video? {
        didSet {
            guard let videoID = video?.videoID else { return }
            playerView.load(withVideoId: videoID)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleSwipe))
        addGestureRecognizer(panGesture)
    }
    
    @objc func handleSwipe(gesture: UIPanGestureRecognizer) {
        if gesture.state == .changed {
            let translation = gesture.translation(in: self.superview)
            self.transform = CGAffineTransform(translationX: 0, y: translation.y)
        } else if gesture.state == .ended {
            let translation = gesture.translation(in: self.superview)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.transform = .identity

                if translation.y > 150 {
                    self.removeFromSuperview()
                }
            })
        }
    }
}
