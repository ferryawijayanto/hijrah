//
//  VideoDataSource.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 24/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class VideoDataSource: NSObject, UITableViewDataSource {
    
    var videos = [Video]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath)
        
        return cell
    }
}
