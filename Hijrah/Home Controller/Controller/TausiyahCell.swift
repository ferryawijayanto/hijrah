//
//  TausiyahCell.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 22/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class TausiyahCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tausiyahImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tausiyahImg.layer.cornerRadius = 10
    }
}
