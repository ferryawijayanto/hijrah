//
//  HomeController.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 22/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    
    var store = TausiyahStore()
    var videoStore = VideoStore()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialize()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "videoTausiyah"?:
            if let selectedIndexPath = tableView.indexPathsForSelectedRows?.first {
                let tausiyah = store.items[selectedIndexPath.row]
                let destinationVC = segue.destination as! VideoController
                destinationVC.videoStore = videoStore
                destinationVC.tausiyah = tausiyah
            }
        default:
            preconditionFailure("Unexpected segue identifier")
        }
    }
}

// MARK: - Private extension
private extension HomeController {
    func initialize() {
        store.fetch()
    }
}

// MARK: - UITableViewDataSource
extension HomeController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return store.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tausiyahCell", for: indexPath) as! TausiyahCell
        
        let item = store.items[indexPath.item]
        
        if
            let name = item.name,
            let imageName = item.image {
                cell.lblName.text = name
                cell.tausiyahImg.image = UIImage(named: imageName)
        }
        return cell
    }    
}
