//
//  VideoController.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 22/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import UIKit

class VideoController: UIViewController {
    
    var store: TausiyahStore!
    var tausiyah: TausiyahItem! {
        didSet {
            navigationItem.title = tausiyah.name
        }
    }
    var videoStore: VideoStore!
    
    let videoDataSource = VideoDataSource()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
    }
}

private extension VideoController {
    
    func initialize() {
        setupTableView()
        fetchSearchVideos()
    }
    
    func setupTableView() {
        tableView.dataSource = videoDataSource
        tableView.delegate = self
    }
    
    func fetchSearchVideos() {
        videoStore.fetchSearchVideos(matching: tausiyah.name ?? "") { (videoResult) in
            switch videoResult {
            case let .success(videos):
                print("Successfully found \(videos.count) video")
                self.videoDataSource.videos = videos
            case let .failure(error):
                print("Error fetching search video: \(error)")
                self.videoDataSource.videos.removeAll()
            }
            self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }
}

extension VideoController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let video = videoDataSource.videos[indexPath.row]
        
        videoStore.fetchImage(for: video) { (imageResults) in
            
            guard
                let videoIndex = self.videoDataSource.videos.firstIndex(of: video),
                case let .success(image) = imageResults else { return }
            
            let photoIndexPath = IndexPath(item: videoIndex, section: 0)
            
            if let cell = tableView.cellForRow(at: photoIndexPath) as? VideoCell {
                cell.videoImgView.image = image
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = videoDataSource.videos[indexPath.row]

        let window = UIApplication.shared.keyWindow!

        let videoPlayerView = Bundle.main.loadNibNamed("PlayerView", owner: self, options: nil)?.first as! VideoPlayerView
        videoPlayerView.frame = view.frame
        videoPlayerView.video = video

        window.addSubview(videoPlayerView)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
