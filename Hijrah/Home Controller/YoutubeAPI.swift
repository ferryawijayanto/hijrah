//
//  YoutubeAPI.swift
//  Hijrah
//
//  Created by Ferry Adi Wijayanto on 23/10/19.
//  Copyright © 2019 Ferry Adi Wijayanto. All rights reserved.
//

import Foundation

enum YoutubeError: Error {
    case invalidJSONData
}

struct YoutubeAPI {
    private static let baseURLString = "https://www.googleapis.com/youtube/v3/search"
    private static let apiKey = "AIzaSyCWDnHYz-cBP8O6Sy18jNknseYL4IlNzxQ"
    
    static func youtubeURL(withAdditionalQuery query: String) -> URL {
        var components = URLComponents(string: baseURLString)!
        var queryItems = [URLQueryItem]()
        
        let baseParams = [
            "part": "snippet",
            "key": apiKey,
            "q": "tausiyah \(query)"
        ]
        
        for (key, value) in baseParams {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        
        components.queryItems = queryItems
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "%20", with: "+")
        
        return components.url!
    }
    
    static func videos(fromJSON data: Data) -> VideoResults {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]

            var finalItems = [Video]()
            
            if let items = jsonObject?["items"] as? Array<Dictionary<String, Any>> {
                for item in items {
                    finalItems.append(Video(dict: item)!)
                }
            }

            if finalItems.isEmpty {
                return .failure(YoutubeError.invalidJSONData)
            }
            
            return .success(finalItems)
        } catch let error {
            return .failure(error)
        }
    }
}
