# Hijrah

Hijrah adalah aplikasi tausiyah di peruntukan bagi user yang baru berhijrah, yang membutuhkan content-content islam untuk selalu bertawakal.

## Home Tab

Ada tiga katagori video tausiyah yang dapat user pilih - Fiqih - Hadist - Tafsir. Video yang akan di tampilkan berasal dari youtube yang di parse dari YoutubeAPI

## Adzan Reminder

Di adzan reminder terdapat informasi tentang waktu adzan di lokasi user saat ini, background pada adzan reminder akan berubah sesuai waktu yang di tentukan dan di sertai notifikasi 10 menit sebelum adzan datang.

## Favorite
Favorite video tab adalah tempat ketika user mem favorite kan video yang telah di pilih pada saat user memilih video di home tab.